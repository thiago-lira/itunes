import axios from 'axios';

const url = 'https://itunes.apple.com/search';

const formatTerm = (term) => term.replace(/[\s]/g, '+');

const iTunesService = {
  searchPodcasts(params) {
    const { term, media } = params;
    return axios.get(url, {
      params: {
        media,
        limit: 200,
        term: formatTerm(term),
      },
    });
  },
};

export default iTunesService;
