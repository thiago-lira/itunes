import React, { createContext, useReducer } from 'react';
import PropTypes from 'prop-types';

const initialValues = {
  isLoading: false,
};

export const PodcastsTypes = {
  ADD_RESULT: 'ADD_RESULT',
  SET_IS_LOADING: 'SET_IS_LOADING',
};

export const PodcastsActions = {
  setIsLoading(isLoading) {
    return {
      type: PodcastsTypes.SET_IS_LOADING,
      payload: isLoading,
    };
  },
  setResult(params) {
    return {
      type: PodcastsTypes.ADD_RESULT,
      payload: params,
    };
  },
};

export function PodcastsReducer(state = initialValues, { type, payload }) {
  switch (type) {
    case PodcastsTypes.ADD_RESULT:
      return { ...state, ...payload };
    case PodcastsTypes.SET_IS_LOADING:
      return { ...state, isLoading: payload };
    default:
      throw new Error(`Unknown type ${type}`);
  }
}

export const PodcastsContext = createContext();

export function PodcastsProvider({ children }) {
  const [podcasts, podcastsDispatch] = useReducer(PodcastsReducer, initialValues);
  return (
    <PodcastsContext.Provider value={{ podcasts, podcastsDispatch }}>
      {children}
    </PodcastsContext.Provider>
  );
}

PodcastsProvider.propTypes = {
  children: PropTypes.element.isRequired,
};
