import React from 'react';
import SearchInput from './components/SearchInput/SearchInput';
import SearchResult from './components/SearchResults/SearchResults';
import { PodcastsProvider } from './store/ducks/podcasts';
import './App.scss';

export default function App() {
  return (
    <div className="container">
      <header>
        <div className="logo">
          <span role="img" aria-label="Headphone logo">
            🎧
          </span>
        </div>
      </header>
      <PodcastsProvider>
        <>
          <section className="search-content">
            <SearchInput />
          </section>
          <section className="main-content">
            <SearchResult />
          </section>
        </>
      </PodcastsProvider>

      <footer />
    </div>
  );
}
