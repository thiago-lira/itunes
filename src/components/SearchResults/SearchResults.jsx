import React, { useContext, useEffect, useState } from 'react';
import SearchResultItem from '../SearchResultItem/SearchResultItem';
import { PodcastsContext } from '../../store/ducks/podcasts';
import Loading from '../Loading/Loading';

import './SearchResults.scss';

export default function SearchResult() {
  const { podcasts } = useContext(PodcastsContext);
  const [list, setList] = useState([]);
  const { isLoading } = podcasts;

  useEffect(() => {
    const { results } = podcasts;
    if (Array.isArray(results)) {
      setList(results);
    }
  }, [podcasts]);

  const items = list.map(({
    artworkUrl100,
    artistName,
    trackId,
    trackName,
  }) => (
    <div key={trackId} className="item">
      <SearchResultItem
        srcCover={artworkUrl100}
        title={artistName}
        description={trackName || 'Não'}
      />
    </div>
  ));

  const results = (
    <section className="results">
      {items}
    </section>
  );

  return isLoading ? <Loading /> : results;
}
