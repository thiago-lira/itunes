import React from 'react';
import './Loading.scss';

export default function Loading() {
  return (
    <div className="loading">
      <div className="loading__icon">
        <span role="img" aria-label="loading">
          💿
        </span>
        <p>Aguarde</p>
      </div>
    </div>
  );
}
