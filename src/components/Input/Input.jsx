import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import './Input.scss';

export default function Input({
  value, onChange, placeholder, sendOnKeyEnter,
}) {
  const handleKeyEnter = useCallback(({ keyCode }) => {
    if (keyCode === 13) {
      sendOnKeyEnter();
    }
  }, [sendOnKeyEnter]);

  return (
    <input
      className="input-element"
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      onKeyUp={sendOnKeyEnter ? handleKeyEnter : null}
    />
  );
}

Input.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  sendOnKeyEnter: PropTypes.func,
};

Input.defaultProps = {
  placeholder: '',
  sendOnKeyEnter: null,
};
