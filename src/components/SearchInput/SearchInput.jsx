import React, { useState, useCallback, useContext } from 'react';
import Input from '../Input/Input';
import Select from '../Select/Select';
import { PodcastsContext, PodcastsActions } from '../../store/ducks/podcasts';
import iTunesService from '../../services/itunes';
import './SearchInput.scss';

const selectOptions = [
  { optionValue: 'all', text: 'Todos os tipos' },
  { optionValue: 'movie', text: 'Filmes' },
  { optionValue: 'podcast', text: 'Podcasts' },
  { optionValue: 'music', text: 'Músicas' },
  { optionValue: 'musicVideo', text: 'Vídeo clipes' },
  { optionValue: 'shortFilm', text: 'curta metragens' },
  { optionValue: 'tvShow', text: 'Séries de tv' },
  { optionValue: 'software', text: 'Softwares' },
  { optionValue: 'ebook', text: 'E-books' },
];

export default function SearchInput() {
  const { podcastsDispatch } = useContext(PodcastsContext);
  const [media, setMedia] = useState('all');
  const [term, setTerm] = useState('');

  const handleClick = () => {
    if (term.length > 0) {
      const { setIsLoading, setResult } = PodcastsActions;
      podcastsDispatch(setIsLoading(true));
      iTunesService.searchPodcasts({ term, media })
        .then(({ data }) => {
          podcastsDispatch(setResult(data));
        })
        .finally(() => {
          podcastsDispatch(setIsLoading(false));
        });
    }
  };

  const handleSelectChange = useCallback(({ target }) => {
    const { value } = target;
    setMedia(value);
  }, [setMedia]);

  const handleTermChange = useCallback(({ target }) => {
    const { value } = target;
    setTerm(value);
  }, [setTerm]);

  return (
    <section className="term">
      <div className="term__input">
        <div>
          <Input
            placeholder="Buscar por mídias"
            value={term}
            onChange={handleTermChange}
            sendOnKeyEnter={handleClick}
          />
        </div>
        <div>
          <button disabled={term.length === 0} type="button" onClick={handleClick}>
            Buscar
          </button>
        </div>
      </div>
      <div className="term__options">
        <span>
          Buscar por:
        </span>
        <Select
          id="select"
          value={media}
          options={selectOptions}
          onChange={handleSelectChange}
        />
      </div>
    </section>
  );
}
