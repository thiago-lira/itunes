import React from 'react';
import PropTypes from 'prop-types';
import './SearchResultItem.scss';

export default function SearchResultItem({ srcCover, title, description }) {
  return (
    <section className="result-item">
      <div className="result-item__cover">
        <img src={srcCover} alt={title} />
      </div>
      <div className="result-item__description">
        <h1>{title}</h1>
        <p>{description}</p>
      </div>
    </section>
  );
}

const { string } = PropTypes;

SearchResultItem.propTypes = {
  srcCover: string.isRequired,
  title: string.isRequired,
  description: string.isRequired,
};
