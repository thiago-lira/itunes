import React from 'react';
import PropTypes from 'prop-types';

export default function Select({ value, options, onChange }) {
  const selectOptions = options.map((option) => {
    const { text, optionValue } = option;
    return (
      <option key={optionValue} value={optionValue}>
        {text}
      </option>
    );
  });

  return (
    <select value={value} onChange={onChange}>
      {selectOptions}
    </select>
  );
}

const {
  string,
  func,
  shape,
  arrayOf,
} = PropTypes;

Select.propTypes = {
  value: string.isRequired,
  onChange: func.isRequired,
  options: arrayOf(shape({
    text: string.isRequired,
    optionValue: string.isRequired,
  })).isRequired,
};
